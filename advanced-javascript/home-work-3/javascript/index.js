'use strict'

class Employee {
	constructor(name, age, salary) {
		this.name = name
		this.age = age
		this.salary = salary
	}

	get empName() {
		return this.name
	}

	set empName(nameEmp) {
		this.name = nameEmp
	}

	get empAge() {
		return this.age
	}

	set empAge(ageEmp) {
		this.age = ageEmp
	}

	get empSalary() {
		return this.salary
	}

	set empSalary(sallaryEmp) {
		this.salary = sallaryEmp
	}
}

const employee = new Employee('Victor', 60, 21000);

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary)
		this.lang = lang
	}
	get empSalary() {
		return this.salary * 3
	}
}
const programmer = new Programmer('Petro', 29, 500, "Moldovanian");

const firstСopyProg = new Programmer('Joshua', 43, 9000, ['Russian', 'Ukrainian', 'English']);
const secondСopyProg = new Programmer('Vanessa', 26, 4000, ['French', 'English']);
const thirdСopyProg = new Programmer('Moses', 3500, 'spiritual immortality', ['Hebrew', 'Aramaic']);

console.log('\n');
console.group("%cObject created from Employee class template", "color: yellow; font-style: italic; font-size: 17px; background-color: red; padding-left: 15px;  padding-right: 15px;")
console.log(employee); // prints the parent object to the console
console.groupEnd()
console.log('\n');

console.group("%cThe object is created from the prototype of the Programmer class, which in turn extends and inherits from the Employee class", "color: yellow; font-style: italic; background-color: red; font-size: 16px; padding-left: 17px;  padding-right: 15px;")
console.log(programmer); // prints to the console the created object from the Programmer class, which in turn is inherited from the Employee class
console.groupEnd()
console.log('\n');

console.group("%cMultiplied employee's salary by 3 times", "color: yellow; font-style: italic; background-color: red; font-size: 17px; padding-left: 15px;  padding-right: 15px;")
console.log(programmer.empSalary); // prints to the console a getter method that multiplies the employee's salary by 3 times
console.log('\n');

console.group("%cA group of created objects inherited from the Programmer class", "color: yellow; font-style: italic; font-size: 17px; background-color: red; padding-left: 15px;  padding-right: 15px;")
console.log(firstСopyProg); // output to the console of newly created workers
console.log(secondСopyProg); // output to the console of newly created workers
console.log(thirdСopyProg); // output to the console of newly created workers
console.groupEnd()
