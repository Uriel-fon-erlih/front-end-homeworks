 ## Prototypal inheritance in Javascript
```
class NewClass { // creating a new entity
	constructor(properties) { // a constructor function that creates a new object, takes parameters (future properties)
		this.theFirstKey = properties.theFirstKey
		this.theSecondKey = properties.theSecondKey
	}

	get parOne() {
		return this.theFirstKey
	}
	set parOne(value) {
		this.theFirstKey = value
	}
	get parTwo() {
		return this.theSecondKey
	}
	set parTwo(value) {
		this.theSecondKey = value
	}
}

const newClass = new NewClass({ // creating a new object based on this class
	theFirstKey: 'value',
	theSecondKey: 'value',
})
```

_Прототипное наследование в `Javascript`, даёт возможность создавать «дочерние» классы объектов (конструкторы), которые **наследуют** признаки из своих «родительских» классов, и таким образом мы можем создавать экземпляры классов на их основе и дополнять их получая тот объект который нам необходим._
