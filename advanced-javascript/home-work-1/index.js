'use strict'

//todo task № 1 { Database }--------------------------------------------------------------------------- //
const clientsOne = ["Gilbert", "Salvatore", "Pirs", "Sommers", "Forbes", "Donovan", "Bennet"];
const clientsTwo = ["Pirs", "Salzman", "Salvatore", "Michaelson"];

function database(one, two) {

	const allClients = [...new Set([...one, ...two])];

	return allClients;
}
const joinArray = database(clientsOne, clientsTwo);

// console.log(joinArray);  // outputting an array to the console



//todo task № 2 { character characteristics }--------------------------------------------------------------------------- //
const characters = [
	{
		name: "Helen",
		lastName: "Gilbert",
		age: 17,
		gender: "woman",
		status: "human"
	},
	{
		name: "Caroline",
		lastName: "Forbes",
		age: 17,
		gender: "woman",
		status: "human"
	},
	{
		name: "Alaric",
		lastName: "Salzman",
		age: 31,
		gender: "man",
		status: "human"
	},
	{
		name: "Damon",
		lastName: "Salvatore",
		age: 156,
		gender: "man",
		status: "vampire"
	},
	{
		name: "Rebecca",
		lastName: "Michaelson",
		age: 1089,
		gender: "woman",
		status: "vempire"
	},
	{
		name: "Claus",
		lastName: "Michaelson",
		age: 1093,
		gender: "man",
		status: "vampire"
	}
];

for (let index = 0; index < characters.length; index++) {
	const object = characters[index];
	// console.log(object); // old version of the object
	function key({ name, lastName, age }) {
		let charactersShortInfo = [];
		charactersShortInfo = { name, lastName, age }
		// console.log(charactersShortInfo); // new version of the object
	}
	key(object);
}



//todo task № 3 { user data }--------------------------------------------------------------------------- //
const userOne = {
	name: "John",
	years: 30
};

const { name, years: age, isAdmin = false } = userOne;

document.body.insertAdjacentHTML('afterbegin',
	`<ul><h3>User Data</h3></h2><li>name: ${name};</user><li>age: ${age};</li><li>isAdmin: ${isAdmin};</li></ul>`);



//todo task № 4 { Satoshi Nakamoto }--------------------------------------------------------------------------- //
const satoshi2018 = {
	name: 'Satoshi',
	surname: 'Nakamoto',
	technology: 'Bitcoin',
	country: 'Japan',
	browser: 'Tor',
	birth: '1975-04-05'
}

const satoshi2019 = {
	name: 'Dorian',
	surname: 'Nakamoto',
	age: 44,
	hidden: true,
	country: 'USA',
	wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
	browser: 'Chrome'
}

const satoshi2020 = {
	name: 'Nick',
	surname: 'Sabo',
	age: 51,
	country: 'Japan',
	birth: '1979-08-21',
	location: {
		lat: 38.869422,
		lng: 139.876632
	}
}

function actualInfo(dossier18, dossier19, dossier20) {

	const fullProfile = { ...dossier18, ...dossier19, ...dossier20 };

	return fullProfile;
}
const satoshiProfile = actualInfo(satoshi2018, satoshi2019, satoshi2020);

// console.log(satoshiProfile);  // outputting actual data about Satoshi Nakamoto to the console



//todo task № 5 { an array of book objects }--------------------------------------------------------------------------- //
const books = [{
	name: 'Harry Potter',
	author: 'J.K. Rowling'
},
{
	name: 'Lord of the rings',
	author: 'J.R.R. Tolkien'
},
{
	name: 'The witcher',
	author: 'Andrzej Sapkowski'
}];

// object to add
const bookToAdd = {
	name: 'Game of thrones',
	author: 'George R. R. Martin'
}

const supplementedBooks = [...books, { ...bookToAdd }];

// console.log(books);  // prints an array of books

// console.log(supplementedBooks); // prints an updated array of books



//todo task № 6 { Vitalii Klichko }--------------------------------------------------------------------------- //
const employee = {
	name: 'Vitalii',
	surname: 'Klichko'
}

const objectAugmented = { ...employee, salary: 1000000, age: 99 };

// console.log(employee); // prints the employee object to the console

// console.log(objectAugmented); // prints the augmented objectAugmented to the console



//todo task № 7 { alert }--------------------------------------------------------------------------- //
const array = ['value', () => 'showValue'];

const [value, showValue] = [...array];

// alert(value); // should output 'value'

// alert(showValue()); // should output 'showValue'
