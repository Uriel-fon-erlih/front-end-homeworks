# try...catch

```
try {
	if (mistake) {
		throw new Error('this error will be output to the console');
	}
	else {
		the code will not end up in the catch block
	}
}
catch (error) {
	console.log(error);  // error output
}
finally {
	this code will be executed to anybody
}
```

`try...catch` _уместно использовать для отлова ошибок, чтобы весь сценарий не останавливался при ошибке, мы кладём сомнительный скрипт в блок_ `try...catch` _и если выпадет ошибка, мы сможем её отловить, при том что скрипт не прекратит свою работу._
