'use strict'

const books = [
	{
		author: "Скотт Бэккер",
		name: "Тьма, что приходит прежде",
		price: 70
	},
	{
		author: "Скотт Бэккер",
		name: "Воин-пророк",
	},
	{
		name: "Тысячекратная мысль",
		price: 70
	},
	{
		author: "Скотт Бэккер",
		name: "Нечестивый Консульт",
		price: 70
	},
	{
		author: "Дарья Донцова",
		name: "Детектив на диете",
		price: 40
	},
	{
		author: "Дарья Донцова",
		name: "Дед Снегур и Морозочка",
	}
];

const div = document.querySelector('#root');

const ul = document.createElement('ul');

div.insertAdjacentElement('afterbegin', ul);

books.map((element) => {
	const { name, price, author } = element;
	const style = ['padding: 1rem;',
		'background: linear-gradient(  rgba(0, 0, 0, 0.8),  white, rgba(0, 0, 0, 0.8));',
		'text-shadow: -1px -1px yellow;',
		'font: 1.3rem/3 Georgia;',
		'font-style: italic;',
		'font-weight: 700;',
		'border-radius: 40px;',
		'color:  rgb(238, 0, 10);'].join('');
	try {
		if (name && price && !author) {
			console.log(`\n`);
			console.log(`\n`);
			console.error(element);
			console.table(element);
			throw new Error(` this object does not contain the required key: 'AUTHOR'`);
		}
		else if (name && author && !price) {
			console.log(`\n`);
			console.log(`\n`);
			console.error(element);
			console.table(element);
			throw new Error(` this object does not contain the required key: 'PRICE'`);
		}
		else if (price && author && !name) {
			console.log(`\n`);
			console.log(`\n`);
			console.error(element);
			console.table(element);
			throw new Error(` this object does not contain the required key: 'NAME'`);
		}
		else {
			return ul.insertAdjacentHTML('afterbegin', `<li>${author}</li><li>${name}</li><li>${price}</li><p></p>`);
		}
	} catch (error) {
		console.log('%c%s', style, error);
		console.table(error);
	}
})
