'use strict'
//* Создать простую HTML страницу с кнопкой Calculate by IP.

//* По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.

//* Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.

//* Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.


window.document.addEventListener('DOMContentLoaded', () => {

	document.querySelector('.button').addEventListener('click', () => {

		fetch('http://api.ipify.org/?format=json')
			.then(response => response.json())
			.then(data => {

				fetch(`http://ip-api.com/json/${data.ip}`)
					.then(response => response.json())
					.then(data => {

						return document.querySelector('.container-info').innerHTML += `
							<div class="location-data">
									<span class="description">Сountry</span>
									<span class="arrow">=></span> ${data.country}
							</div>
							<div class="location-data">
									<span class="description">Region name</span>
									<span class="arrow">=></span> ${data.regionName}
							</div>
							<div class="location-data">
									<span class="description">City</span>
									<span class="arrow">=></span> ${data.city}
							</div>
							<div class="location-data">
									<span class="description">Region number</span>
									<span class="arrow">=></span> ${data.region}
							</div>
							<div class="location-data">
									<span class="description">District</span>
									<span class="arrow">=></span>
									<span class="error">Not Found</span>
							</div>
						`
					})
			})
			.catch(error => console.log(error))

	},
		{
			once: true
		})

})
