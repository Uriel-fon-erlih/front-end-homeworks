'use strict'
//todo Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.

//todo Отправить AJAX запрос по адресу https://swapi.dev/api/films и получить список всех фильмов серии Звездные войны

//todo Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.

//todo Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран.

//todo Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).

//todo Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.

//todo Все запросы на сервер необходимо выполнить с помощью async await.

window.addEventListener('DOMContentLoaded', () => {

	const url = 'https://swapi.dev/api/films/';

	async function getResponse(url) {

		let response = await fetch(url)
		let jsonData = await response.json()
		let data = await jsonData.results
		let list = document.querySelector('.list')

		for (const key in data) {
			// console.log(data[key].characters); // массивы персонажей

			list.innerHTML += `
				<li class="item">
						<h3 class="item__title">
								<span class="title-text">Movie title =>
								</span>
									"${data[key].title}"
						</h3>
						<p class="episode">
								<span class="title-text">Episode number =>
								</span>
									${data[key].episode_id}
						</p>
						<p class="characters">
								<span class="title-text">Character list =>
								</span>
									${data[key].characters}
						</p>
						<p class="short-content">
								<span class="title-text">Short content =>
								</span>
									${data[key].opening_crawl}
						</p>
				</li>
			`
			// data[key].characters.map((link) => {
			// 	fetch(link)
			// 		.then((data) => data.json())
			// 		.then((link) => {
			// 			let name = link.name
			// 			let characters = document.querySelector('.characters')
			// 			let element = `<li>-- ${name}</li>`;
			// 			return characters.insertAdjacentHTML('beforeend', element);
			// 		})
			// })
		}
	}

	getResponse(url)
})
