'use strict'
let canvas = document.querySelector('#myCanvas');
let ctx = canvas.getContext("2d"); //** контекст отрисовки canvas
let x = canvas.width / 2; //** положение мяча по оси X
let y = canvas.height - 30; //** положение мяча по оси Y
let dx = 2; //** перемещение мяча на каждой итерации по оси X в px
let dy = -2; //** перемещение мяча на каждой итерации по оси Y в px
let ballRadius = 10; //** радиус мяча
let paddleHeight = 10; //** высота биты
let paddleWidth = 75; //** ширина биты
let paddleX = (canvas.width - paddleWidth) / 2; //** положение биты, начальная точка на оси X
let rightPressed = false;  //** инициализация правой кнопки
let leftPressed = false; //** инициализация левой кнопки
//** кирпичи
let brickRowCount = 3;
let brickColumnCount = 5;
let brickWidth = 75;
let brickHeight = 20;
let brickPadding = 10;
let brickOffsetTop = 30;
let brickOffsetLeft = 30;
let score = 0; //** результаты игры
let lives = 3; //** количество жизней
let winner = document.querySelector('.winner');
let loading = document.querySelector('.loading');

let bricks = [];
for (let c = 0; c < brickColumnCount; c++) {
	bricks[c] = [];
	for (let r = 0; r < brickRowCount; r++) {
		bricks[c][r] = { x: 0, y: 0, status: 1 };
	}
}

document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
document.addEventListener("mousemove", mouseMoveHandler, false);

function mouseMoveHandler(e) {
	let relativeX = e.clientX - canvas.offsetLeft;
	if (relativeX > 0 && relativeX < canvas.width) {
		paddleX = relativeX - paddleWidth / 2;
	}
}

function keyDownHandler(event) {
	if (event.keyCode == 39) {
		rightPressed = true;
	}
	else if (event.keyCode == 37) {
		leftPressed = true;
	}
}

function keyUpHandler(event) {
	if (event.keyCode == 39) {
		rightPressed = false;
	}
	else if (event.keyCode == 37) {
		leftPressed = false;
	}
}

function randColor() {
	let r = Math.floor(Math.random() * (256)),
		g = Math.floor(Math.random() * (256)),
		b = Math.floor(Math.random() * (256));
	return '#' + r.toString(16) + g.toString(16) + b.toString(16);
}

function collisionDetection() {
	for (let c = 0; c < brickColumnCount; c++) {
		for (let r = 0; r < brickRowCount; r++) {
			let b = bricks[c][r];
			if (b.status == 1) {
				if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
					dy = -dy;
					b.status = 0;
					score++;
					if (score == brickRowCount * brickColumnCount) {
						setTimeout(() => {
							winner.insertAdjacentText('afterbegin', 'YOU WIN, CONGRATULATIONS!');
							loading.insertAdjacentText('afterbegin', 'the game restarts...');
							setTimeout(() => canvas = null, 1000);
							return setInterval(() => {
								winner.style.color = randColor();
								document.body.style.background = randColor();
								setTimeout(() => document.location.reload(), 5000);
							}, 150);
						});
					}
				}
			}
		}
	}
}

function drawScore() {
	ctx.font = "16px Arial";
	ctx.fillStyle = "#0095DD";
	ctx.fillText("Score: " + score, 8, 20);
}

function drawLives() {
	ctx.font = "16px Arial";
	ctx.fillStyle = "#0095DD";
	ctx.fillText("Lives: " + lives, canvas.width - 65, 20);
}

function drawBall() {
	ctx.beginPath();
	ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
	ctx.fillStyle = "#0095DD";
	ctx.fill();
	ctx.closePath();
}

function drawPaddle() {
	ctx.beginPath();
	ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
	ctx.fillStyle = "#0095DD";
	ctx.fill();
	ctx.closePath();
}

function drawBricks() {
	for (let c = 0; c < brickColumnCount; c++) {
		for (let r = 0; r < brickRowCount; r++) {
			if (bricks[c][r].status == 1) {
				let brickX = (c * (brickWidth + brickPadding)) + brickOffsetLeft;
				let brickY = (r * (brickHeight + brickPadding)) + brickOffsetTop;
				bricks[c][r].x = brickX;
				bricks[c][r].y = brickY;
				ctx.beginPath();
				ctx.rect(brickX, brickY, brickWidth, brickHeight);
				ctx.fillStyle = "#0095DD";
				ctx.fill();
				ctx.closePath();
			}
		}
	}
}

function draw() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	drawBricks();
	drawBall();
	drawPaddle();
	drawScore();
	drawLives();
	collisionDetection();

	x += dx;
	y += dy;

	if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
		dx = -dx;
	}

	if (y + dy < ballRadius) {
		dy = -dy;

	} else if (y + dy > canvas.height - ballRadius) {
		if (x > paddleX && x < paddleX + paddleWidth) {
			dy = -dy;
		} else {
			lives--;
			if (!lives) {
				alert("GAME OVER");
				document.location.reload();
			} else {
				x = canvas.width / 2;
				y = canvas.height - 30;
				dx = 2;
				dy = -2;
				paddleX = (canvas.width - paddleWidth) / 2;
			}
		}
	}

	if (rightPressed && paddleX < canvas.width - paddleWidth) {
		paddleX += 7;
	}
	else if (leftPressed && paddleX > 0) {
		paddleX -= 7;
	}

	requestAnimationFrame(draw);
}
draw();



//**  палитра цветов
for (let i = 0; i < 6; i++) {
		for (let j = 0; j < 6; j++) {
	  ctx.fillStyle = `rgb(
				${Math.floor(255 - 42.5 * i)},
			${Math.floor(255 - 42.5 * j)},
			0)`;
	  ctx.fillRect(j * 25, i * 25, 25, 25);
	}
 }

